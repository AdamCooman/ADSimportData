function htfres = LSSSextractHTFs(varargin)
% this function extracts the HTFs from LSSS simulation results
%
%   [htfres,HTFs,ssfreq,fsys,diffind] = LSSSextractHTFs(struct,basis);
%
% struct is a structure that contains the simulation results of an LSSS simulation.
% as imported by ADSimportData.

p=inputParser();
% simulation result, as imported by ADSimportSimData
p.addRequired('simres',@checkSimres);
% You can choose the basis functions of the HTFs, if basis is 'exp' (default) 
% then classic HTFs are returned, if basis is 'cos' then a sine and cosine
% basis is used.
p.addParameter('basis','exponent',@checkBasis);
p.parse(varargin{:});
args=p.Results;

% assign the correct basis now
[~,args.basis]=checkBasis(args.basis);

% check the provided struct
requiredFields = {'sweepvar_ssfreq','Mix1','Mix2','freq'};

% extract the frequency axis
htfres.freq = args.simres.sweepvar_ssfreq;
F = length(htfres.freq);

MIX = args.simres.Mix1.*args.simres.Mix2;
FOL = args.simres.Mix2==-1;

% H is the amount of HTFs.
H = (size(MIX,2)-1)/2;
% remove all the extra fields from the struct
args.simres = rmfield(args.simres,requiredFields);
% get the remaining fields
fields = fieldnames(args.simres);

% generate the index vectors for later
[~,IND] = sort(MIX,2);
TAR = zeros(2,F*H);
ORI = zeros(2,F*H);
ind = 0;
for ff=1:F
    rng = ind+1:(ind+2*H+1);
    ORI(1,rng) = ff;
    ORI(2,rng) = IND(ff,:);
    TAR(1,rng) = 1:(2*H+1);
    TAR(2,rng) = ff;
    ind = ind+2*H+1;
end
ORI = sub2ind([F 2*H+1],ORI(1,:),ORI(2,:));
TAR = sub2ind([2*H+1 F],TAR(1,:),TAR(2,:));


% preallocate the result struct
for ii=1:length(fields)
    htfres.(fields{ii}) = zeros(2*H+1,F);
end

% assign the HTFs to their matrices

for ii=1:length(fields)
    htfres.(fields{ii})(TAR) = args.simres.(fields{ii})(ORI);
end

% also reshape the FOL struct
FOL(TAR) = FOL(ORI);

% take the complex conjugate of the HTFs that are folded
for ii=1:length(fields)
	htfres.(fields{ii})(FOL) = conj(htfres.(fields{ii})(FOL));
end


%% if needed, change basis
switch args.basis
    case {'sine','cosine'}
        % transform the HTFs into the sine and cosine basis
        for ss=1:length(fields)
            for hh=1:H
                Hk  = htfres.(fields{ss})(hh,:);
                Hmk = htfres.(fields{ss})(end-hh+1,:);
                htfres.(fields{ss})(hh,:) = Hk+Hmk;
                htfres.(fields{ss})(end-hh+1,:) = 1i*(Hk-Hmk);
            end
            
        end
	case  'exponent'
		% do nothing
	otherwise
		error('basis unknown');
end

end

function checkSimres(simres)
if ~isstruct(simres)
	error('simres should be a struct');
end
requiredFields = {'sweepvar_ssfreq','Mix1','Mix2','freq'};
if ~all(isfield(simres,requiredFields))
    msg = [sprintf('The provided simulation result struct does not contain the required fields\n The following fields are needed:\n') sprintf(' - %s\n',requiredFields{:})];
    error('ADSimportData:fieldsmissing',msg);
end
end
function [tf,basis] = checkBasis(basis)
basis=validatestring(basis,{'exponent','sine','cosine'});
tf = true;
end